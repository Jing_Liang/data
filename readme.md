Each file is a state:

format: [observation, done(boolean if robot is in collision or reached max step), states]

observation: [lidar, rgb, depth, goal(distance, angle in robot coordinate), robot velocity] 

states: [list of other_state, robot position]

other_state: .name: name of the obstacle
	     	 .pos: position of the obstacle
	     	 .vel: velocity of the obstacle

position type: http://docs.ros.org/api/geometry_msgs/html/msg/Pose.html
			usage: other_state.pos.position.x, other_state.pos.position.y, other_state.pos.position.z ...
velocity type: http://docs.ros.org/api/geometry_msgs/html/msg/Twist.html
			usage: other_state.vel.linear.x, other_state.vel.linear.y ...